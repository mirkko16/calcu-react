import Boton from "./Boton";
import React, { Component, useState } from 'react';


class Calculadora extends Component {

    constructor(props) {
        super(props)
        this.state = {
            data: '',
            result: ''
        }
    }
    onClick = button => {
        if (button === "=") {
            this.calcular()
        }
        else if (button === "C") {
            this.reset()
        }
        else if (button == '+' || button == '-') {
            this.setState({
                data: this.state.data + button,
                result: this.state.result + '@' + button + '@'
            })
        }
        else {
            this.setState({
                data: this.state.data + button,
                result: this.state.result + button
            })
        }
    };


    calcular = () => {
        const data = this.state.result.split('@')

        console.log(data)
        let sum = 0
        let resu = Number(data[0])
        while(sum != data.length) {
            switch(data[sum + 1]) {
                case '+': 
                    resu = resu + Number(data[sum + 2])
                    sum += 3
                    break;
                case '-': 
                    resu = resu - data[sum + 2]
                    sum += 3
                    break;
                case '*': 
                    resu = resu * data[sum + 2]
                    sum += 3
                    break;
            }
        }

        console.log(resu)



        // var resultado = ''
        // if (this.state.result.includes('--')) {
        //     resultado = this.state.result.replace('--', '+')
        // }

        // else {
        //     resultado = this.state.result
        // }

    };

    reset = () => {
        this.setState({
            result: ""
        })
    };

    render() {
        let { data } = this.state;
        return (
            <div className="boton">

                <Boton click={this.onClick} />

                {/* <Boton operador={this.Operado} visor={this.Visor} /> */}

                <div className="resultado">

                    <p>{data}</p>
                </div>
            </div>
        );
    }
}


export default Calculadora;
